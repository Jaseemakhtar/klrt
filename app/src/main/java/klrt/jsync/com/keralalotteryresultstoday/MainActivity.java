package klrt.jsync.com.keralalotteryresultstoday;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends  AppCompatActivity implements View.OnClickListener{
    WebView     webView;
    ImageView imgBack, imgForward, imgRefresh;
    ProgressBar progressBar;
    String url;

    AlertDialog.Builder  alertDialg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        url = "http://www.keralalotteries.info";

        webView = findViewById(R.id.webView);
        imgBack = findViewById(R.id.btn_back);
        imgForward = findViewById(R.id.btn_forward);
        imgRefresh = findViewById(R.id.btn_refresh);
        imgRefresh.setOnClickListener(this);
        imgForward.setOnClickListener(this);
        imgBack.setOnClickListener(this);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new myWebClient());
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.loadUrl(url);

        progressBar = findViewById(R.id.progressBar);

        alertDialg = new AlertDialog.Builder(MainActivity.this);
        alertDialg.setCancelable(false);
        alertDialg.setMessage("Do you want to exit?");
        alertDialg.setTitle("Confirmation");
        alertDialg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });
        alertDialg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });


    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()){
            webView.goBack();
        }else {
            alertDialg.show();

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_back:
                if (webView.canGoBack()){
                    webView.goBack();
                }
                break;
            case R.id.btn_forward:
                if (webView.canGoForward()){
                    webView.goForward();
                }
                break;
            case R.id.btn_refresh:
                webView.reload();
                break;
        }
    }

    public  class myWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            imgBack.setVisibility(View.INVISIBLE);
            imgForward.setVisibility(View.INVISIBLE);
            imgRefresh.setVisibility(View.INVISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            progressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            imgRefresh.setVisibility(View.VISIBLE);
            if (view.canGoForward()){
                imgForward.setVisibility(View.VISIBLE);
            }
            if (view.canGoBack()){
                imgBack.setVisibility(View.VISIBLE);
            }
        }



    }

}
